<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EquipmentControllerTest extends TestCase
{

    use RefreshDatabase;

    private $itemsTestArray = [
        'data' => [],
        'items' => [
            'chests' => [['id' => 1, 'name' => 'chest', 'image' => 'image chest', 'price' => 9.99]],
            'prizes' => [
                [
                    'id' => 2,
                    'name' => 'prize',
                    'image' => 'image prize',
                    'code' => 'code',
                    'price' => 9.99,
                    'status' => 'waiting'
                ]
            ],
            'runes' => [['id' => 3, 'name' => 'rune', 'image' => 'image rune', 'bonus' => 1]]
        ]
    ];

    public function setUp(): void
    {
        parent::setUp();

        $this->seed('ChestSeeder');
        $this->seed('PrizeSeeder');
        $this->seed('RuneSeeder');
        $this->seed('EquipmentSeeder');
    }

    /**
     * Get empty equipment
     *
     * @return void
     */
    public function testGetEmptyEquipment()
    {
        $response = $this->get('/api/equipment/1');

        $response
            ->assertOk()
            ->assertExactJson($this->itemsTestArray);
    }

    /**
     * Get equipment with items inside
     *
     * @return void
     */
    public function testGetNonEmptyEquipment()
    {
        // seed example items to equipment
        $this->seed('EquipmentItemSeeder');

        $response = $this->get('/api/equipment/1');

        $this->itemsTestArray['data'] = [1,2,3];

        $response
            ->assertOk()
            ->assertExactJson($this->itemsTestArray);
    }

    /**
     * Get non exist equipment
     *
     * @return void
     */
    public function testGetNotExistEquipment()
    {
        $response = $this->get('/api/equipment/99');

        $response
            ->assertStatus(404)
            ->assertExactJson([]);
    }

    /**
     * Add item to equipment correct
     *
     * @return void
     */
    public function testItemAddedToEquipmentCorrect()
    {
        $response = $this->postJson('/api/equipment/', ['item_id' => 1, 'equipment_id' => 1]);

        $response
            ->assertCreated()
            ->assertExactJson([]);
    }

    /**
     * Add item (buy) to equipment incorrect
     *
     * @return void
     */
    public function testItemAddedToEquipmentIncorrect()
    {
        // Item dont exist
        $response = $this->postJson('/api/equipment/', ['item_id' => 99, 'equipment_id' => 1]);

        $response->assertStatus(422);

        // Equipment dont exist
        $response = $this->postJson('/api/equipment/', ['item_id' => 1, 'equipment_id' => 99]);

        $response->assertStatus(422);

        // Equipment and Item dont exist
        $response = $this->postJson('/api/equipment/', ['item_id' => 99, 'equipment_id' => 99]);

        $response->assertStatus(422);
    }
}
