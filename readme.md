# Plan



## Opis / założenia

systemu ekwipunku backend. Ekwipunek składa się z:
a) lista skrzynek
b) lista run 
c) lista nagród 



* Skrzynka posiada atrybuty: nazwa, obrazek, cena
* Runa posiada atrybuty: nazwa, obrazek, bonus
* Nagroda posiada atrybuty: nazwa, obrazek, kod, cena, status (0-oczekujący, 1-wysłany, 2-odrzucony)



## Funkcjonalność

a) Pobranie całego ekwipunku
b) Kupowanie skrzynki, runy, nagrody



# Diagram

![](readme_file/diagram.png)



## Relacje

* equipment - item - wiele to wielu (https://laravel.com/docs/7.x/eloquent-relationships#many-to-many)
* item -> chest / rune / prize https://laravel.com/docs/7.x/eloquent-relationships#one-to-one-polymorphic-relations



### equipment

* id - int
* user_id - int (by nie było pusto, puste tabele / obiekty to zło)



### equipment_item

* id - int
* equipment_id - int
* item_id - int



### item

* id - int
* name - string
* image - string (dla uproszczenia)
* iterable_id - int
* iterable_type - string (nazwa obiektu - https://laravel.com/docs/7.x/eloquent-relationships#one-to-one-polymorphic-relations)



### chest

* id - int
* price - decimal



### rune

* id - int
* bonus - decimal (?)



### prize

* id - int
* code - string
* price - decimal
* status - enum (0-waiting, 1-sent, 2-rejected)



# Działanie



## Pobranie całego ekwipunku

* Pobieramy ekwipunek po ID ekwiupunku
* Podczepaimy pod response liste wszystkich, wszystkich itemów - by moc te info wykorzystac w formach na frontendzie oraz do wyswietlenia ekwipunku. Za to liste rzeczy w ekwipunku mozemy jedynie pod samych id'kach pobrać
* Trzeba podzielić liste wszystkich itemów dostępnych ze względu na typ (na resourceach) - oraz jedna kolekcja utkana pod to zapytanie



### Testy

* Pobieranie ekwipunku bez itemow
* Pobieranie ekwipunku po ssedowaniu  - zapełnionego
* Pobieranie ekwipunku po id który nie istnieje - error



## Kupowanie skrzynki, runy, nagrody

* Kupowanie po post, podawanie id ekwipunku oraz itemu



### Testy

* Poprawne dodanie itemu do ekwipunku
* Nie poprawne dodanie do ekwipunku - zły id ekwipunku, zły id itemu, oba złe, nieistniejące



## ToDo

1. Przygotować wcześniej strukturę projketu
2. Struktura testów
3. Pierw budujemy item oraz jego wszystkie formy, migracje tworzymy, relacje
4. Potem equipment wraz z relacjami i migracjami
5. Kontroler i routy, resourcy
6. Testujemy, seedy pod testy



## Note

* https://laravel.com/docs/7.x/eloquent-resources
* https://laravel.com/docs/7.x/eloquent-resources#resource-collections
* https://laravel.com/docs/7.x/validation#form-request-validation
* https://laravel.com/docs/7.x/controllers#resource-controllers
* up docker: docker-compose up nginx mysql phpmyadmin
* PHPMyAdmin: http://localhost:8069
* dostep do bash'a : `docker-compose exec workspace bash`

MySQL
- server: mysql
- user: root
- pass: root
- database: gamehag

### Set and install docker

1. cd laradock
2. cp env-example .env
3. docker-compose up -d nginx mysql phpmyadmin
4. docker-compose exec workspace bash
5. composer install
6. cp .env.example .env
7. artisan key:generate
8. exit
9. cd ..
10. sudo chmod -R 777 storage bootstrap/cache
11. cd laradock
12. edit .env:
`Find MySQL section and edit MYSQL_ROOT_PASSWORD equals to your new password.`
13. edit docker-compose.yml
`Find MySQL section, in it you have a volume section. You need to change the first line:`
${DATA_SAVE_PATH}/mysql/project_name:/var/lib/mysql
14. docker-compose down && docker-compose up  nginx mysql phpmyadmin
15. http://localhost:8080 - phpmyadmin, login, add new database
16. edit laravel .env
DB_HOST=mysql
DB_PASS=
17. docker-compose exec  workspace bash - migrate itp





