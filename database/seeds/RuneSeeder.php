<?php

use Illuminate\Database\Seeder;
use App\Item;

class RuneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Item::createRune('rune', 'image rune', 1);
    }
}
