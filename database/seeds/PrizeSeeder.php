<?php

use Illuminate\Database\Seeder;
use App\Item;
class PrizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Item::createPrize('prize', 'image prize', 'code', 9.99, 'waiting');
    }
}
