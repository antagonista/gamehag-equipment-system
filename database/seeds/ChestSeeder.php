<?php

use Illuminate\Database\Seeder;
use App\Item;

class ChestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Item::createChest('chest', 'image chest', 9.99);
    }
}
