<?php

use Illuminate\Database\Seeder;
use App\Equipment;
use App\Item;

class EquipmentItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $equipment = Equipment::find(1);

        // Chest item
        $item = Item::find(1);

        $equipment->items()->save($item);

        // Rune item
        $item = Item::find(2);

        $equipment->items()->save($item);

        // Prize item
        $item = Item::find(3);

        $equipment->items()->save($item);
    }
}
