<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ChestSeeder::class);
        $this->call(RuneSeeder::class);
        $this->call(PrizeSeeder::class);
        $this->call(EquipmentSeeder::class);
        $this->call(EquipmentItemSeeder::class);
    }
}
