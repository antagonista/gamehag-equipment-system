<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    /**
     * Handle for Items relations
     */
    public function items()
    {
        return $this->belongsToMany('App\Item');
    }
}
