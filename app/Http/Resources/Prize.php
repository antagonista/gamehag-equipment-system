<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Prize extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (int) $this->item->id,
            'name' => (string) $this->item->name,
            'image' => (string) $this->item->image,
            'code' => (string) $this->code,
            'price' => (double) $this->price,
            'status' => (string) $this->status
        ];
    }
}
