<?php

namespace App\Http\Resources;

use App\Chest;
use App\Http\Resources\Chest as ChestResources;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Rune;
use App\Http\Resources\Rune as RuneResources;
use App\Prize;
use App\Http\Resources\Prize as PrizeResources;

class ItemCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    /**
     * Add to response items
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function with($request)
    {
        return [
            'items' => [
                'chests' => ChestResources::collection(Chest::all()),
                'runes' => RuneResources::collection(Rune::all()),
                'prizes' => PrizeResources::collection(Prize::all())
            ]
        ];
    }
}
