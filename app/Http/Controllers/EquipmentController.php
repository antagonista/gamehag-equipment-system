<?php

namespace App\Http\Controllers;

use App\Equipment;
use App\Http\Resources\ItemCollection;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\StoreEquipmentItem;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Add (buy) item to equipment
     *
     * @param StoreEquipmentItem $request
     * @return JsonResponse
     */
    public function store(StoreEquipmentItem $request)
    {
        $equipment = Equipment::find($request->equipment_id);
        $item = Item::find($request->item_id);

        if ($equipment === null || $item === null) {
            return response()->json(null, 422);
        } else {
            $equipment->items()->save($item);

            return response()->json(null, 201);
        }
    }

    /**
     * Get one Equipment with Items data
     *
     * @param  int  $id
     * @return ItemCollection|JsonResponse
     */
    public function show($id)
    {
        $equipment = Equipment::find($id);

        if ($equipment === null) {
            return response()->json(null, 404);
        } else {
            return new ItemCollection($equipment->items);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
