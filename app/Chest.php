<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chest extends Model
{
    /**
     * Handle item relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function item()
    {
        return $this->morphOne('App\Item', 'itemable');
    }
}
