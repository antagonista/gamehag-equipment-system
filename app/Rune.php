<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rune extends Model
{
    /**
     * Handle item relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function item()
    {
        return $this->morphOne('App\Item', 'itemable');
    }
}
