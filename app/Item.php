<?php

namespace App;

use App\Chest;
use App\Rune;
use Illuminate\Database\Eloquent\Model;
use App\Prize;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Item extends Model
{
    /**
     * Handle item relation
     *
     * @return MorphTo
     */
    public function itemable()
    {
        return $this->morphTo();
    }

    /**
     * Create Chest Item
     *
     * @param $name
     * @param $image
     * @param $price
     */
    public static function createChest($name, $image, $price)
    {
        $chest = new Chest(['price' => $price]);
        $chest->save();
        $chest->item()->save(new Item(['name' => $name, 'image' => $image]));
    }

    /**
     * Create Rune Item
     *
     * @param $name
     * @param $image
     * @param $bonus
     */
    public static function createRune($name, $image, $bonus)
    {
        $rune = new Rune(['bonus' => $bonus]);
        $rune->save();
        $rune->item()->save(new Item(['name' => $name, 'image' => $image]));
    }

    /**
     * Create Prize item
     *
     * @param $name
     * @param $image
     * @param $code
     * @param $price
     * @param $status
     */
    public static function createPrize($name, $image, $code, $price, $status)
    {
        $prize = new Prize(['code' => $code, 'price' => $price, 'status' => $status]);
        $prize->save();
        $prize->item()->save(new Item(['name' => $name, 'image' => $image]));
    }
}
