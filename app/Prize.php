<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Prize extends Model
{
    /**
     * Handle item relation
     *
     * @return MorphOne
     */
    public function item()
    {
        return $this->morphOne('App\Item', 'itemable');
    }
}
